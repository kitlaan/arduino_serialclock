# Arduino Serial Clock

A clock that is powered by USB, which also gets its clock sync
via that USB (TTY) connection.

# Linux Setup Notes

Create the script that sets the time. (Currently ignore sync requests.)
```
#!/bin/sh
# /usr/local/bin/arduino-clock

[ ! -e /dev/ttyACM.clock ] && exit 0

logger writing to clock
echo T$(date +%s) > /dev/ttyACM.clock
```

Make sure the permissions are set properly:
```
sudo chown :dialout /usr/local/bin/arduino-clock
```

Symlink this so we also get a daily cron job.
```
sudo ln -s /usr/local/bin/arduino-clock /etc/cron.daily/
```

Create a udev rule so this can be uniquely identified.
(this all has to be a single line!)
```
SUBSYSTEM=="tty", ATTRS{idVendor}=="1b4f", ATTRS{idProduct}=="9205",
    ATTRS{serial}=="11223344556677889900",
    GROUP="dialout", MODE="0660",
    SYMLINK +="ttyACM.clock", RUN+="/usr/local/bin/arduino-clock"
```