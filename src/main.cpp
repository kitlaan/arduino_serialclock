#include <Arduino.h>

// so USB shows a unique device node
#include "USBMCUSerialNum.h"
USBSerialNum USBSN(NULL);

#include <TimeLib.h>
#include <Timezone.h>

#include <Adafruit_GFX.h>
#include <Adafruit_LEDBackpack.h>

TimeChangeRule tcrEDT = {"EDT", Second, Sun, Mar, 2, -240};
TimeChangeRule tcrEST = {"EST", First, Sun, Nov, 2, -300};
Timezone tz(tcrEDT, tcrEST);

#define DISPLAY_24H

Adafruit_7segment matrix = Adafruit_7segment();
#define BRIGHTNESS 0  // 0 - 15

#define MSG_TIME_HEADER   "T"
#define MSG_TIME_REQUEST  0x07

uint8_t syncLoading = 0;

////////////////////////////////////////////////////////////////////////////////

uint8_t indexToPos(uint8_t ix)
{
    return (ix < 2) ? ix : (ix + 1);
}

void drawLoading()
{
    for (uint8_t ix = 0; ix < 4; ix++) {
        matrix.writeDigitRaw(indexToPos(ix), (ix < syncLoading) ? 0x80 : 0x00);
    }
    syncLoading = (syncLoading + 1) % 5;

    matrix.writeDisplay();
}

void drawClock()
{
    time_t tmLocal = tz.toLocal(now());

    int lHour;
    int lMinute;
    int lSecond;

#ifdef DISPLAY_24H
    lHour = hour(tmLocal);
#else
    lHour = hourFormat12(tmLocal);
#endif

    lMinute = minute(tmLocal);
    lSecond = second(tmLocal);

    matrix.writeDigitNum(0, lHour / 10);
    matrix.writeDigitNum(1, lHour % 10);
    matrix.drawColon((lSecond % 2) == 0);
    matrix.writeDigitNum(3, lMinute / 10);
    matrix.writeDigitNum(4, lMinute % 10);

    matrix.writeDisplay();
}

void processSyncMessage()
{
    const uint32_t VALID_TIME = 1357041600; // Jan 1, 2013 GMT
    uint32_t epoch;

    if (Serial.find((char*)MSG_TIME_HEADER)) {
        epoch = Serial.parseInt();
        if (epoch >= VALID_TIME) {
            setTime(epoch);
        }
    }
}

time_t requestSync()
{
    // async request
    Serial.write(MSG_TIME_REQUEST);
    return 0;
}

////////////////////////////////////////////////////////////////////////////////

void setup()
{
    Serial.begin(115200);

    setSyncProvider(requestSync);

    matrix.begin(0x70);
    matrix.setBrightness(BRIGHTNESS);
    matrix.writeDigitRaw(0, 1<<4); // just so we know that we've set up
    matrix.writeDisplay();

    //while (!Serial); // needed for leonardo
    //Serial.println(USBSN.getSerialNum());
}

void loop()
{
    if (Serial.available()) {
        processSyncMessage();
    }

    if (timeStatus() != timeNotSet) {
        drawClock();
    }
    else {
        drawLoading();
    }

    delay(250);
}
